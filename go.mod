module gitlab.com/golib4/mnemonic-generator

go 1.22.2

require (
	github.com/tyler-smith/go-bip39 v1.1.0
	golang.org/x/crypto v0.22.0
)
