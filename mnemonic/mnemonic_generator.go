package mnemonic

import (
	"crypto/hmac"
	"crypto/sha512"
	bip392 "github.com/tyler-smith/go-bip39"
	"golang.org/x/crypto/pbkdf2"
	"strings"
)

const Size12 = 128
const Size24 = 256

const (
	_TonIterations   = 100000
	_TonBasicSalt    = "TON Seed version"
	_TonPasswordSalt = "TON fast Seed version"
)

func GenerateMnemonic(password string, size int) (string, error) {
	outputError := make(chan error)
	outputMnemonic := make(chan *mnemonic)
	for i := 0; i <= 10; i++ {
		go generateMnemonicPart(size, password, outputMnemonic, outputError)
	}
	select {
	case err := <-outputError:
		return "", err
	case mnemonicData := <-outputMnemonic:
		return mnemonicData.string(), nil
	}
}

func generateMnemonicPart(size int, password string, outputMnemonic chan<- *mnemonic, outputError chan<- error) {
	for {
		entropy, err := bip392.NewEntropy(size)
		if err != nil {
			outputError <- err
			return
		}
		words, err := bip392.NewMnemonic(entropy)
		if err != nil {
			outputError <- err
			return
		}
		mnemonicData := &mnemonic{Seed: strings.Split(words, " "), Password: password}
		if mnemonicData.isTonMnemonicStandard() {
			outputMnemonic <- mnemonicData
		}
	}
}

type mnemonic struct {
	Seed     []string
	Password string
}

func (m *mnemonic) string() string {
	return strings.Join(m.Seed, " ")
}

func (m *mnemonic) isTonMnemonicStandard() bool {
	hash := m.passwordHash()
	if len(m.Password) > 0 {
		p := pbkdf2.Key(hash, []byte(_TonPasswordSalt), 1, 1, sha512.New)
		if p[0] != 1 {
			return false
		}
	} else {
		p := pbkdf2.Key(hash, []byte(_TonBasicSalt), _TonIterations/256, 1, sha512.New)
		if p[0] != 0 {
			return false
		}
	}
	return true
}

func (m *mnemonic) passwordHash() []byte {
	mac := hmac.New(sha512.New, []byte(m.string()))
	mac.Write([]byte(m.Password))
	return mac.Sum(nil)
}
